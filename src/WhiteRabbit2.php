<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
     $coins = array(
                '1'   => 0,
                '2'   => 0,
                '5'   => 0,
                '10'  => 0,
                '20'  => 0,
                '50'  => 0,
                '100' => 0
            );
			//reverse the array while preserving the index
			$preserved = array_reverse($coins, true);
			
			$reste=$amount;
			
			foreach($preserved as $key => $value){
				if($reste >= $key){
					//value is amount of coins that the rest of amount contains
					$value=(int) ($reste / $key);
					//new reste is the modulo of previous reste by the type of coin which is the $key here
					$reste = $reste % $key;
					//update the array with number of type of coin checked in this loop
					$coins[$key]=$value;
				}
			}
			//return the resulting array of amount of each type of coin  required to fulfill the amount
			return $coins;
    }
}