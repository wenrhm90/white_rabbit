<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        //filter text and leave ony lowercase alphabets  
        return strtolower(preg_replace("/[^A-Za-z]/", "", file_get_contents($filePath)));
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        //count each letter's occurrences 
        $lettercount = count_chars($parsedFile, 1);
        //sort array by ascendant occurences 
        asort($lettercount);
        //get letters in an array
        $keys = array_keys($lettercount);
        //count all values of the array
        $Count = count($keys);

        //find the medianletter 
        if ($Count % 2 == 0) {
            
            $medianLetterAscii = $keys[$Count / 2];

            $occurrences = $lettercount[$medianLetterAscii];
            return chr($medianLetterAscii);
        } 
    }
}